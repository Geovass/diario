-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema diary
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema diary
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `diary` DEFAULT CHARACTER SET utf8 ;
USE `diary` ;

-- -----------------------------------------------------
-- Table `diary`.`Calificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`Calificacion` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `valor` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `diary`.`Entrada`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `diary`.`Entrada` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `texto` TEXT NOT NULL,
  `fecha` DATETIME NOT NULL,
  `Calificacion_id` INT UNSIGNED NOT NULL,
  `visibilidad` TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_Entrada_Calificacion_idx` (`Calificacion_id` ASC) VISIBLE,
  CONSTRAINT `fk_Entrada_Calificacion`
    FOREIGN KEY (`Calificacion_id`)
    REFERENCES `diary`.`Calificacion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
